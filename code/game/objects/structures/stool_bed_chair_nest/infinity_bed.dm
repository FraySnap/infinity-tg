/obj/structure/stool/bed/psych
	name = "psych bed"
	desc = "For prime comfort during psychiatric evaluations."
	icon_state = "psychbed"
	icon = 'icons/obj/infinity_object.dmi'

/obj/structure/stool/bed/double
	name = "Double bed"
	desc = "The best bed in deep space!"
	icon_state = "double"
	icon = 'icons/obj/infinity_object.dmi'

/obj/structure/stool/bed/double/right
	name = "Double bed"
	desc = "The best bed in deep space!"
	icon_state = "double1"

/obj/structure/stool/bed/double/left
	name = "Double bed"
	desc = "The best bed in deep space!"
	icon_state = "double2"
